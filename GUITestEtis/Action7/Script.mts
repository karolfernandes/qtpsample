﻿Browser("FordEtis home page").Page("FordEtis home page").Link("Vehicle").Click @@ hightlight id_;_Browser("FordEtis home page").Page("FordEtis home page").Link("Vehicle")_;_script infofile_;_ZIP::ssf1.xml_;_
DataTable.SetCurrentRow(1)
val_vin = DataTable.Value("vin","Global")

Browser("FordEtis home page").Page("Vehicle Lookup").WebEdit("vin").Set(val_vin) @@ hightlight id_;_Browser("FordEtis home page").Page("Vehicle Lookup").WebEdit("vin")_;_script infofile_;_ZIP::ssf2.xml_;_
Browser("FordEtis home page").Page("Vehicle Lookup").WebButton("Search").Click @@ hightlight id_;_Browser("FordEtis home page").Page("Vehicle Lookup").WebButton("Search")_;_script infofile_;_ZIP::ssf3.xml_;_
Browser("FordEtis home page").Page("Vehicle Summary").Check CheckPoint("Vehicle Summary") @@ hightlight id_;_Browser("FordEtis home page").Page("Vehicle Summary")_;_script infofile_;_ZIP::ssf4.xml_;_
Browser("FordEtis home page").Page("Vehicle Summary").WebTable("Primary Features").Check CheckPoint("Primary Features") @@ hightlight id_;_Browser("FordEtis home page").Page("Vehicle Summary").WebTable("Primary Features")_;_script infofile_;_ZIP::ssf5.xml_;_
